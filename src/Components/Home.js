import React from 'react'
import Payment from './Payment';
import { Link } from 'react-router-dom';

function Home() {
  return (
    <div className='back'>
      <h1 style={{ textAlign: 'center', fontWeight: 'bold', fontSize: '5em' }}>Sleeve 2</h1>
      <h2 style={{ textAlign: 'center', fontWeight: 'bold', fontSize: '2em' }}>The ultimate music accessory for your Mac.</h2>
      <p style={{ textAlign: 'center', fontSize: '1em' }}>Sleeve sits on the desktop, displaying and controlling the music you’re currently playing in <br></br>
        <img src='/apple.jpg' alt='Icon' style={{ height: '20px', width: '30px' }} />
        Apple Music,
        <img src='/spotify.jpg' alt='Icon' style={{ height: '20px', width: '30px' }} />
        Spotify, and
        <img src='/doppler.png' alt='Icon' style={{ height: '30px', width: '30px' }} />
        Doppler.</p><br></br>
      <div align='center'>
        <button>Mac App Store</button>
        <button><center><Payment></Payment></center></button>

      </div> <br></br>
      <p><center>No subscriptions. No in-app <br></br> purchases. Requires macOS 11 Big Sur <br></br> or later.</center></p>  <br></br>
      <div align='center'>
      <Link to="/New">
      <button>
        <img src='/Sleeve 2.png' alt='Icon' style={{ height: '40px', width: '40px' }} />
        <span>See what's new in Sleeve 2.3</span>
      </button>
    </Link>
      </div>
      <br></br> <br></br>
      <h6 color='#0000FF'><center>NEW IN 2.0</center></h6>
      <h1 align='center'>Themes. Unlimited themes.</h1>
      <p align='center'>Themes in Sleeve make creating and switching between customizations easy. Share your own <br></br> creations with friends and install as many themes as you like with just a double-click.</p>
      <div class="pip">
        <div class="pip-item">
          <img src="/theme1.jpg" alt="First" />
          <div class="title"><b>Eternal Sunshine  <br></br> Slow Magic</b></div>
        </div>
        <div class="pip-item">
          <img src="/theme2.jpg" alt="Second" />
          <div class="title"><b>Bunny Is A Rider <br></br> Caroline Polachek</b></div>
        </div>
        <div class="pip-item">
          <img src="/theme3.jpg" alt="Third" />
          <div class="title"><b>Are You Listening? <br></br>Chelsea Cutler</b></div>
        </div>
        <div class="pip-item">
          <img src="/theme4.jpg" alt="Fourth" />
          <div class="title"><b>Creation Comes Alive <br></br>Petit Biscuit</b></div>
        </div>
      </div>
      <br></br>

      <h5 align="center" color="blue">CUSTOMIZATION</h5>
      <h1 align="center"><b>Countless ways to customize.</b></h1>
      <p align="center">Customization is at the core of the Sleeve experience — choose from any <br></br> combination of design choices, behaviors and settings to make Sleeve at home on <br></br> your desktop.</p>
      <br></br>
      <div align="center">
        <img src='/images/app9.png' alt='app' />
        <img src='/images/app3.png' alt='app' />
        <img src='/images/app2.png' alt='app' />
        <img src='/images/app6.png' alt='app' />
        <img src='/images/app8.png' alt='app' />
        <img src='/images/app5.png' alt='app' />
        <img src='/images/app4.png' alt='app' />
        <img src='/images/app7.png' alt='app' />
        <img src='/images/app1.png' alt='app' />
      </div><br></br> <br></br>

      <div className="artwork-container">
        <div className="artwork-title-container">

          <div className="artwork-title" align='center'>
            <img src='/images/app8.png ' alt='Icon' style={{ height: '40px', width: '40px' }} />
            ART-WORK</div>
          <p align='center'>Scale artwork all the way up or all the way down.Round the corners or leave them square.<br></br>
            Choose shadow and lighting effects to bring your album artwork to life.</p>
          <p align='center'>Or hide it completely.</p>
        </div>
        <div className="artwork-cards">
          <div class="art-card">
            <div class="art-item">
              <img src="/images/Art1.jpg" alt="First" />
            </div></div>
          <div class="art-card">
            <div class="art-item">
              <img src="/images/Art2.jpg" alt="Second" />
            </div></div>
          <div class="art-card">
            <div class="art-item">
              <img src="/images/Art3.jpg" alt="Third" />
            </div>
          </div>
        </div>
      </div>
      <br></br> <br></br>
      <div className="artwork-container">
        <div className="artwork-title-container">
          <div className="artwork-title" align='center'>
            <img src='/images/app6.png ' alt='Icon' style={{ height: '40px', width: '40px' }} />
            TYPOGRAPHY</div>
          <p align='center'>Pick the track info you want to display, and then exactly how to display it.</p>
          <p align='center'>Choose the fonts, weights, sizes, and transparency to use for each line, along with customizing color and shadow.</p>
        </div>
        <div className="artwork-cards">
          <div class="art-card">
            <div class="art-item">
              <img src="/images/Typo1.jpg" alt="First" />
            </div></div>
          <div class="art-card">
            <div class="art-item">
              <img src="/images/Typo2.jpg" alt="Second" />
            </div></div>
        </div>
      </div>
      <br></br> <br></br>
      <div className="artwork-container">
        <div className="artwork-title-container">
          <div className="artwork-title" align='center'>
            <img src='/images/app4.png ' alt='Icon' style={{ height: '40px', width: '40px' }} />
            <img src='/images/app7.png ' alt='Icon' style={{ height: '40px', width: '40px' }} />
            <img src='/images/app3.png ' alt='Icon' style={{ height: '40px', width: '40px' }} />
            INTERFACE</div>
          <p align='center'>Customize the layout, alignment and position to fit your setup.</p>
          <p align='center'>Show and hide playback controls. Add a backdrop layer and customize it.</p>
        </div>
        <div className="artwork-cards">
          <div class="art-card">
            <div class="art-item">
              <img src="/images/interface1.jpg" alt="First" />
            </div></div>
          <div class="art-card">
            <div class="art-item">
              <img src="/images/interface2.jpg" alt="Second" />
            </div></div>
        </div>
      </div>
      <br></br> <br></br>
      <div className="artwork-container">
        <div className="artwork-title-container">
          <div className="artwork-title" align='center'>
            <img src='/images/app9.png ' alt='Icon' style={{ height: '40px', width: '40px' }} />
            <img src='/images/app2.png ' alt='Icon' style={{ height: '40px', width: '40px' }} />
            <img src='/images/app1.png ' alt='Icon' style={{ height: '40px', width: '40px' }} />
            SETTINGS</div>
          <p align='center'>Decide if Sleeve stays out of the way, behind windows, or in front of them — or only when you need to see it.<br></br>
            Show it in the Dock, choose from custom icons, or keep it on the Desktop only.</p>
          <p align='center'>Set your custom keyboard shortcuts and integrate with the apps you use.</p>
        </div>
        <div className="artwork-cards">
          <div class="art-card">
            <div class="art-item">
              <img src="/images/Settings1.jpg" alt="First" />
            </div></div>
          <div class="art-card">
            <div class="art-item">
              <img src="/images/Settings2.jpg" alt="Second" />
            </div></div>
          <div class="art-card">
            <div class="art-item">
              <img src="/images/Settings3.jpg" alt="Third" />
            </div>
          </div>
        </div>
      </div>
      <h4 align='center'>INTEGRATIONS</h4>
      <h2 style={{ textAlign: 'center', fontWeight: 'bold', fontSize: '2em' }}>Like, Scrobble.</h2>


    </div>
  )
}

export default Home


